/* mythface: a quality mythtv interface */
#define _GNU_SOURCE
#include <glib.h>
#include <gtk/gtk.h>
#include <gmyth/gmyth.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <unique/unique.h>

enum {
	TIME_COLUMN,
	TITLE_COLUMN,
	SUBTITLE_COLUMN,
	RECORDINFO_COLUMN,
	N_COLUMNS
};

/* Libunique commands */
enum {
	/* Null command */
	MYTHFACE_CMD_0,
	/* Command to raise mythface */
	MYTHFACE_CMD_RAISE,
};

typedef struct {
	GtkWindow *main_window;
	GtkEntry *filter_box;
	GtkWidget *thetree;
	GtkScrolledWindow *scrolled_window;

	GMythBackendInfo *backend;

	/* Timeout for when we can actually filter the tree */
	guint filter_timeout_id;
	/* Whether the last filter text change was clicking the clear button */
	gboolean filter_clear_button;

	/* The time of the latest recording currently in the list */
	uint64_t latest_start_time;
	/* latest_start_time that gets updated whilst adding to the list */
	uint64_t latest_start_time_tmp;

	/* Data store for the recordings */
	GtkListStore *store;

	/*** The three models ***/
	/* This one contains *all* our data */
	GtkTreeModel* base_model;
	/* This one filters the data */
	GtkTreeModel *filtered_model;
	/* This is the model on top of the filtered data */
	GtkTreeModel *sorted_filtered_model;
} mythface_t;

/*** List related functions ***/
/* Populates the treeview */
static void create_list( mythface_t *mythface );
/* Adds a program to the list */
static void list_append( gpointer data,
			 gpointer user_data );
static gboolean filter_func( GtkTreeModel *model, GtkTreeIter *iter, gpointer _filter_box );

/* Update the recordings list from the database */
static void update_recordings( mythface_t *mythface );

/* Callback for timeout to update recordings */
static gboolean list_update_timeout( gpointer _mythface );

/* Returns a list of recorded programs.
   The list is of RecordedInfo pointers */
static GList* ls_recorded_files( mythface_t *mythface )
{
	GMythScheduler *scheduler;
	GList          *list = NULL;
	gint            res = 0;

	scheduler = gmyth_scheduler_new();

	if (gmyth_scheduler_connect_with_timeout(scheduler,
						 mythface->backend, 10) == FALSE)
	{
		g_warning("Could not connect to backend db");
		goto cleanup;
	}

	res = gmyth_scheduler_get_recorded_list(scheduler, &list);
	if (res < 0) {
		g_warning("Could not retrieve recorded list");
		list = NULL;
		goto cleanup1;
	}

	if (res == 0) {
		g_print("No files were found in the backend.\n");
		list = NULL;
		goto cleanup1;
	}

cleanup1:
	gmyth_scheduler_disconnect(scheduler);
cleanup:
	g_object_unref(scheduler);

	return list;
}

#define obj(t, n) do {							\
	mythface-> n = t(gtk_builder_get_object( builder, #n ));	\
	} while (0)

static void init_ui( mythface_t *mythface )
{
	GtkBuilder *builder;

	/* Initialise all the fields in the mythface_t struct */
	mythface->filter_timeout_id = -1;
	mythface->filter_clear_button = FALSE;
	mythface->latest_start_time = 0;
	mythface->latest_start_time_tmp = 0;
	mythface->store = NULL;

	builder = gtk_builder_new();
	g_assert( gtk_builder_add_from_file( builder, "mythface.gtk", NULL ) );
	gtk_builder_connect_signals( builder, mythface );

	obj( GTK_WINDOW, main_window );

	obj( GTK_ENTRY, filter_box );
	/* Set the filter box's clear button to be inactive */
	gtk_entry_set_icon_sensitive( mythface->filter_box,
				      GTK_ENTRY_ICON_SECONDARY,
				      FALSE );

	obj( GTK_WIDGET, thetree );
	obj( GTK_SCROLLED_WINDOW, scrolled_window );

	g_object_unref( G_OBJECT(builder) );
	gtk_widget_show( GTK_WIDGET(mythface->main_window) );

	mythface->backend = gmyth_backend_info_new_full( "benjy",
							 "mythtv",
							 "beards123",
							 "mythconverg",
							 0 );
	if( mythface->backend == NULL )
		g_error("Couldn't connect to backend\n");

	create_list( mythface );
	update_recordings( mythface );
}

#undef obj

static UniqueResponse cb_unique_msg( UniqueApp *app,
				     gint command,
				     UniqueMessageData *message_data,
				     guint time_,
				     gpointer _mythface )
{
	mythface_t *mythface = _mythface;

	gtk_window_present( mythface->main_window );
	update_recordings( mythface );

	return UNIQUE_RESPONSE_OK;
}

mythface_t mythface;

int main( int argc, char** argv )
{
	UniqueApp *app;
	gtk_init(&argc, &argv);

	app = unique_app_new_with_commands( "org.gnome.MythFace", NULL,
					    "raise", MYTHFACE_CMD_RAISE,
					    NULL );

	if( unique_app_is_running(app) ) {
		g_debug( "Already running" );
		unique_app_send_message( app, 1, NULL );
		return 0;
	}

	g_signal_connect( G_OBJECT(app), "message-received",
			  G_CALLBACK(cb_unique_msg), &mythface );

	init_ui( &mythface );

	/* Enqueue the recordings list update timeout */
	g_timeout_add_seconds( 60, list_update_timeout, &mythface );

	gtk_main();
	return 0;
}

static void create_list( mythface_t *mythface )
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;

	/* Add the columns */
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes ("Time",
							   renderer,
							   "text", TIME_COLUMN,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (mythface->thetree), column);
	gtk_tree_view_column_set_sort_column_id( column, TIME_COLUMN );

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes ("Title",
							   renderer,
							   "text", TITLE_COLUMN,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (mythface->thetree), column);
	gtk_tree_view_column_set_sort_column_id( column, TITLE_COLUMN );


	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes ("Subtitle",
							   renderer,
							   "text", SUBTITLE_COLUMN,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (mythface->thetree), column);
	gtk_tree_view_column_set_sort_column_id( column, SUBTITLE_COLUMN );


	mythface->store = gtk_list_store_new( N_COLUMNS,
					      G_TYPE_STRING,
					      G_TYPE_STRING,
					      G_TYPE_STRING,
					      G_TYPE_POINTER );

	mythface->base_model = gtk_tree_model_sort_new_with_model( GTK_TREE_MODEL(mythface->store) );

	/* Set up the filtering of the tree */
	mythface->filtered_model = gtk_tree_model_filter_new( mythface->base_model, NULL );
	gtk_tree_model_filter_set_visible_func( GTK_TREE_MODEL_FILTER( mythface->filtered_model ),
						filter_func, mythface->filter_box, NULL );

	/* Now create a sorted model based on the filtered model */
	mythface->sorted_filtered_model = gtk_tree_model_sort_new_with_model ( mythface->filtered_model );
	gtk_tree_view_set_model( GTK_TREE_VIEW(mythface->thetree), mythface->sorted_filtered_model );

	gtk_tree_sortable_set_sort_column_id( GTK_TREE_SORTABLE(mythface->sorted_filtered_model),
					      TIME_COLUMN, GTK_SORT_DESCENDING );

}

static void list_append( gpointer _ri,
			 gpointer _mythface )
{
	GtkTreeIter iter;
	RecordedInfo *ri = (RecordedInfo*)_ri;
	mythface_t *mythface = _mythface;
	struct tm st;
	char* ds = NULL;
	/* µs since epoch */
	uint64_t t;
	assert(ri!= NULL && mythface->store != NULL);

	t = ((uint64_t)ri->start_time->tv_sec) * 1000000;
	t += ri->start_time->tv_usec;

	if( t > mythface->latest_start_time_tmp )
		mythface->latest_start_time_tmp = t;

	if( t <= mythface->latest_start_time ) {
		/* Don't add this entry, it's old */
		g_free( ri );
		return;
	}

	assert(localtime_r( &(ri->start_time->tv_sec), &st ) != NULL);

	asprintf( &ds, "%4.4i/%2.2i/%2.2i %2.2i:%2.2i", st.tm_year+1900,
						  st.tm_mon+1,
						  st.tm_mday,
                                                  st.tm_hour, st.tm_min );

	gtk_list_store_append (mythface->store, &iter );
	gtk_list_store_set( mythface->store, &iter,
			    TIME_COLUMN, ds,
			    TITLE_COLUMN, ri->title->str,
			    SUBTITLE_COLUMN, ri->subtitle->str,
			    RECORDINFO_COLUMN, (gpointer)ri,			    
			    -1 );
	free(ds);
}

static gboolean filter_func( GtkTreeModel *model,
		      GtkTreeIter  *iter,
		      gpointer     _filter_box )
{
	GtkEntry *filter_box = _filter_box;
	const gchar *filter_text;
	gchar *title_text;
	gchar *description_text;
	gchar **words;
	guint i = 0;
	gboolean pass = TRUE;

	gtk_tree_model_get( model, iter, TITLE_COLUMN, &title_text, -1 );
	gtk_tree_model_get( model, iter, SUBTITLE_COLUMN, &description_text, -1 );
	filter_text = gtk_entry_get_text( filter_box );

	/* Split the filter string at every space chr to get an array of filter 'words' */
	words = g_strsplit( filter_text, " ", 1000 );

	/* Check that all of the filter string words are in both
	   the title and description. */
	while ( pass && words[i] != NULL )
	{
		if( strcasestr( title_text, words[i] ) == NULL
		    && strcasestr( description_text, words[i] ) == NULL )
			pass = FALSE;

		i++;
	}

	g_strfreev( words );
	return pass;
}

G_MODULE_EXPORT void play_button_clicked_cb( GtkButton *button,
					     gpointer user_data )
{
	GtkTreeIter iter;
	GtkTreeModel *model;
	GtkTreeSelection *sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(mythface.thetree));
	RecordedInfo *ri;

	if( gtk_tree_selection_get_selected(sel, &model, &iter) ) {
		GError *err = NULL;
		gchar* argv[] = { "/usr/bin/mplayer", "-fs","-cache", "10000", "", NULL };

		gtk_tree_model_get( model, &iter, RECORDINFO_COLUMN, &ri, -1 );
		
		g_print( "%s\n", ri->basename->str );
		asprintf( &argv[4], "/mnt/video/%s", ri->basename->str );

		if( !g_spawn_async(NULL, /* working dir */
				   argv, /* argv */
				   NULL, /* env */
				   0,    /* flags */
				   NULL, /* child_setup */
				   NULL, /* user_data */
				   NULL, /* child pid */
				   &err) )
			g_warning("Failed to spawn mplayer: %s", err->message);

		free( argv[4] );

	}
}

static gboolean filter_timeout( gpointer _mythface )
{
	mythface_t *mythface = _mythface;
	GtkAdjustment *v;

	gtk_tree_model_filter_refilter( GTK_TREE_MODEL_FILTER(mythface->filtered_model) );

	/* Scroll to the top of the scroll window */
	v = gtk_scrolled_window_get_vadjustment( mythface->scrolled_window );
	gtk_adjustment_set_value( v, gtk_adjustment_get_lower(v) );

	mythface->filter_timeout_id = -1;
	return FALSE;
}

G_MODULE_EXPORT void cb_entry_changed( GtkEditable *entry,
				       gpointer _mythface )
{
	mythface_t *mythface = _mythface;
	const gchar *filter_text = gtk_entry_get_text( mythface->filter_box );

	gtk_entry_set_icon_sensitive( mythface->filter_box,
				      GTK_ENTRY_ICON_SECONDARY,
				      strlen(filter_text) != 0 );

	if( mythface->filter_timeout_id > 0 ) {
		/* Unregister the current timeout, and shift it to be later */
		g_source_remove( mythface->filter_timeout_id );
		mythface->filter_timeout_id = -1;
	}

	if( mythface->filter_clear_button ) {
		/* Go straight to filtering */
		filter_timeout(mythface);
		return;
	}

	mythface->filter_timeout_id = g_timeout_add( 300, filter_timeout, mythface );
	g_assert( mythface->filter_timeout_id > 0 );
}

G_MODULE_EXPORT void cb_filter_icon_press( GtkEntry *entry,
					   GtkEntryIconPosition icon_pos,
					   GdkEvent *event,
					   gpointer _mythface )
{
	mythface_t *mythface = _mythface;

	mythface->filter_clear_button = TRUE;
	gtk_entry_set_text( entry, "" );
}

static void update_recordings( mythface_t *mythface )
{
	GList *reclist = ls_recorded_files( mythface );

	if( reclist == NULL ) {
		g_warning( "Couldn't get a list of recordings from database :(" );
		return;
	}

	g_list_foreach( reclist, list_append, mythface );
	mythface->latest_start_time = mythface->latest_start_time_tmp;

	/* Discard the list -- data that was unused was freed in list_append */
	g_list_free(reclist);
}

static gboolean list_update_timeout( gpointer _mythface )
{
	mythface_t *mythface = _mythface;
	update_recordings( mythface );

	return TRUE;
}
